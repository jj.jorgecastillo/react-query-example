import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "./index.css";
import { QueryClientProvider, QueryClient } from "@tanstack/react-query";
/**
 * import { QueryClientProvider, QueryClient } from "@tanstack/react-query";
 * Esto seria el contexto que debe envolver la app.
 * */

import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
/**
 * import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
 * Esto seria una herramienta opcional solo para desarrollo. Debugger.
 * */

const queryClient = new QueryClient();
/**
 * const queryClient = new QueryClient();
 * Instanciamos de QueryClient(), para establecer donde se van a guardar los
 * datos en memoria cache.
 * */

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <App />
      <ReactQueryDevtools initialIsOpen={false} panelPosition="left" />
    </QueryClientProvider>
  </React.StrictMode>
);
