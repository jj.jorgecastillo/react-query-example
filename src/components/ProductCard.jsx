import React from "react";

export default function ProductCard({ product, onDelete, onUpdate }) {
  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <h3>{product.name}</h3>
        <div>
          <input
            type="checkbox"
            checked={product.inStock}
            id={product.id}
            onChange={(e) =>
              onUpdate.mutate({
                ...product,
                inStock: e.target.checked,
              })
            }
          />
          <label
            style={{ color: "green", fontSize: 14, fontWeight: 600 }}
            htmlFor={product.id}
          >
            En almacén
          </label>
        </div>
      </div>

      <p>
        Valor:
        <b>{product.price}</b> $
      </p>
      <p>
        <i>{product.description}</i>
      </p>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          marginTop: 20,
        }}
      >
        <div>
          <button
            style={{ background: "#cf1313" }}
            onClick={() => onDelete.mutate(product.id)}
          >
            Eliminar
          </button>
        </div>
      </div>
    </div>
  );
}
