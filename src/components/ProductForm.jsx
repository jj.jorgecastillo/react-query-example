import { useMutation, useQueryClient } from "@tanstack/react-query";
import { createProduct } from "../api/productsApi";

/** useMutation;
 *  Para modificar los datos en el servidor.
 *  post, put, delete.
 * */

/** = useMutation({  mutationFn,  onSuccess  })
 *  mutationFn: funcion que se encarga de hacer la petición al server (post, put, delete)
 *  onSuccess: funcion que nos permite ver el resultado despues que se cumplió la ejecución,
 * */

const ProductForm = () => {
  const queryClient = useQueryClient();

  const addProductMutation = useMutation({
    mutationFn: createProduct,
    onSuccess: () => {
      // invalidate cache and refetch
      // como decir la cache ya no es valida, refrescate. Tipo como llamar de nuevo a un useEffect
      // elimina o compara los datos de la memoria cache trae los datos nuevos.
      // queryKey: ["products"] ?? ese mismo es el que deseo invalidar y por eso le paso el key
      queryClient.invalidateQueries({ queryKey: ["products"] });
    },
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);

    console.log("formData", formData);
    const product = Object.fromEntries(formData);
    console.log("product", product);

    // Ejecutamos el (mutationFn)  con el nombre que le definimos (addProductMutation).
    // mandamos lo datos por medio del ".mutate"

    addProductMutation.mutate({
      ...product,
      inStock: true,
    });
  };

  return (
    <form
      onSubmit={handleSubmit}
      style={{
        display: "flex",
        flexDirection: "column",
        width: 400,
        justifyContent: "space-between",
      }}
    >
      <div style={{ padding: 10, display: "flex", justifyContent: "space-between" }}>
        <label style={{ margin: "5px 60px" }} htmlFor="name">
          Nombre
        </label>
        <input type="text" name="name" id="name" />
      </div>
      <div
        style={{
          padding: 10,
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <label style={{ margin: "5px 60px" }} htmlFor="price">
          Precio
        </label>
        <input type="number" name="price" id="price" />
      </div>
      <div
        style={{
          padding: 10,
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <label style={{ margin: "5px 60px" }} htmlFor="description">
          Descripcion
        </label>
        <textarea name="description" id="description" />
      </div>
      <div
        style={{
          padding: 10,
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <div>
          <button
            style={{ margin: "20px auto 0", backgroundColor: "magenta", width: 130 }}
            type="reset"
          >
            Reiniciar
          </button>
        </div>
        <div>
          <button
            style={{ margin: "20px auto 0", backgroundColor: "green", width: 130 }}
            type="submit"
          >
            Enviar
          </button>
        </div>
      </div>
    </form>
  );
};

export default ProductForm;
