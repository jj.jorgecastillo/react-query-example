import { useQuery, useMutation, useQueryClient } from "@tanstack/react-query";
import { deleteProduct, getProducts, updateProduct } from "../api/productsApi";

const Products = () => {
  /** ESTADOS DE LA CONSULTA.
   *  const { isLoading, data, isError, error } = useQuery
   *
   * El products contiene algunos estados muy importantes que deberá tener en cuenta para ser productivo.
   * Una consulta solo puede estar en uno de los siguientes estados en un momento dado:
      isLoading  0 status === 'loading'- La consulta aún no tiene datos
      isError 0 status === 'error'- La consulta encontró un error
      isSuccess 0 status === 'success'- La consulta fue exitosa y los datos están disponibles

      Más allá de esos estados primarios, hay más información disponible según el estado de la consulta:
        error- Si la consulta está en un isErrorestado, el error está disponible a través de la errorpropiedad.
      data- Si la consulta está en un successestado, los datos están disponibles a través de la datapropiedad.

   * los estados estados que conocemos de los otros hooks
   * isLoading = Si la consulta aun esta cargando
   * data = Son los datos que regresa la consulta,
   * isError = Si el servidor respondio este seria false.
   * error = Si ocurrió un error este seria el que responde.
   * 
   * Así que tenga en cuenta que una consulta puede estar en loadingestado sin obtener datos. Como una regla de oro:
      El status da información sobre data: ​​¿Tenemos alguno o no?
      El fetchStatus da información sobre queryFn: ​​¿Está funcionando o no?
   * */

  /** KEYS Y FN PARA HACER LAS PETICIONES.
   * = useQuery({ queryKey: ["products"], queryFn: getProducts })
   *
   * queryKey: ["products"] en corchetes para la petición.
   * queryFn = Seria para invocar la funcion que haría la petición.
   * select: (data) => data.sort((a, b) => b.id - a.id), es para hacer mostrar los datos mas nuevos ordenados.
   * */

  const queryClient = useQueryClient();

  const cargar = () => queryClient.invalidateQueries("products");

  const {
    isLoading,
    data: products,
    isError,
    error,
    fetchStatus,

    // status //
  } = useQuery({
    queryKey: ["products"],
    queryFn: getProducts,
    select: (data) => data.sort((a, b) => b.id - a.id),
    enabled: false,
  });

  const deleteProductMutation = useMutation({
    mutationFn: deleteProduct,
    onSuccess: () => {
      queryClient.invalidateQueries(["products"]);
    },
  });

  const updateProductMutation = useMutation({
    mutationFn: updateProduct,
    onSuccess: () => {
      queryClient.invalidateQueries("products");
    },
  });
  console.log(fetchStatus);
  /**
   *  Podriamos manejar el loading y el error antes renderizar nuestros datos.
   * */

  if (fetchStatus === "idle") {
    return (
      <div>
        <button onClick={cargar}>Cargar</button>
      </div>
    );
  }
  if (isLoading) {
    return <div>Cargando...</div>;
  }

  if (isError) {
    return <div>Error: {error.message}</div>;
  }

  // usando status:
  /**
    if (status === "loading") {
      return <span>Loading...</span>;
    }

    if (status === "error") {
      return <span>Error: {error.message}</span>;
    }
   * */

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        width: 700,
        flexWrap: "wrap",
      }}
    >
      {!products.length ? (
        <div>No tienes productos</div>
      ) : (
        products.map((product) => (
          <div
            key={product.id}
            style={{
              background: "lightcoral",
              color: "gainsboro",
              padding: 20,
              borderRadius: 20,
              margin: "20px auto 10px",
              borderTop: "5px solid #fbc5c59e",
              width: 300,
            }}
          >
            <ProductCard
              product={product}
              onDelete={deleteProductMutation}
              onUpdate={updateProductMutation}
            />
          </div>
        ))
      )}
    </div>
  );
};

export default Products;
