import ProductForm from "./components/ProductForm";
import Products from "./components/Products";

const App = () => {
  return (
    <>
      <div
        style={{
          border: "3px solid #ed7671e3",
          padding: 40,
          borderRadius: 24,
          width: "100%",
        }}
      >
        <ProductForm />
      </div>
      <h2>Lista de productos</h2>
      <hr />
      <Products />
    </>
  );
};

export default App;
